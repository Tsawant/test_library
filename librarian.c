#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void add_book(){
	// input books details from the user.
	book_t b;
	book_accept(&b);
	// write books details into the users file.
	book_add(&b);
	book_display(&b);
}

void find_book(){
	char name[30];
	book_t b;
	printf("Enter Book name: ");
	scanf("%s",name);
	if(book_find_by_name(&b, name) == 1){
		printf("Book %s is available.\n", name);
		book_display(&b);
	    return;
	}
	else
	{
		printf("Book %s is not available.", name);
	}
	
}
void librarian_area(user_t *u) {
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
					add_book();
				break;
			case 5:
					find_book();
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 11:
				break;
			case 12:
				break;
			case 13:
				break;
		}
	}while (choice != 0);		
}

